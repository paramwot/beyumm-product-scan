## Project Name: "beyumm-product-scan"
***
## This repository contains files for extracting ids and making request from database file

* `extract_id_notebook.ipynb` notebook contains code to extract id from database
* `main.py` contains code to create txt for status code zero/one
* `barcode_ids.txt` has all the barcode_ids
* `status_one.txt` has all the barcode_ids which status code is ___one___
* `status_zero.txt` has all the barcode_ids which status code is ___zero___
* `products.sql` is the main database file
