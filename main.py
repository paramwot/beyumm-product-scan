# Required Libraries
import pandas as pd
import requests
from concurrent import futures
import time

# reading txt file to create barcode_id array
file = open("barcode_ids.txt", "r")
barcodes = file.read()
barcode_id_list = barcodes.split('\n')  # List containing barcode_ids
file.close()
# print(len(barcode_id_list))

url_list = []  # Created url_list: contains request url
for code in barcode_id_list:
    url = f'https://world.openfoodfacts.org/api/v0/product/{code}.json'
    url_list.append(url)
# print(len(url_list))

status_one_list = []  # List contains ids for status code:1
status_zero_list = []  # List contains ids for status code:0


def get_response_data(url_link):
    """
    function which gives get response from API
    :param url_link:
    :return: None
    """
    response = requests.get(url_link)  # GET Method
    if response.json()['status'] == 1:
        status_one_list.append(int(response.json()['code']))
    elif response.json()['status'] == 0:
        status_zero_list.append(int(response.json()['code']))


# ThreadPool for handaling multiple API calls
with futures.ThreadPoolExecutor(max_workers=100) as executor:
    start = time.time()
    res = executor.map(get_response_data, url_list)
    end = time.time()

print(f'Time: {(end - start)/60} minutes')  # Printing execution time in minutes
print(f'Zero: {len(status_zero_list)}')  # Printing length of status_zero_list
print(f'One: {len(status_one_list)}')  # Printing length of status_one_list

# Writing txt file from array of status code one and zero
# for status_one_list
textfile_for_one = open("status_one.txt", "w")
for element in status_one_list:
    textfile_for_one.write(str(element) + "\n")
textfile_for_one.close()

# for status_zero_list
textfile_for_zero = open("status_zero.txt", "w")
for element in status_zero_list:
    textfile_for_zero.write(str(element) + "\n")
textfile_for_zero.close()
